//Version: v1.0.0
package constant

//define error code
const (
	ERRCODE1  = "SVCM000001"
	ERRCODE2  = "SVCM000002"
	ERRCODE3  = "SVCM000003"

	ERRCODE4  = "SV21000004"
)

const (
	SV100021 = "SV100021"
)

const (
	RSA = "rsa"
	RSA256PUB = "RSA256-PUB"
)
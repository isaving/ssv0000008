//Version: v1.0.0
package controllers

import (
	"git.forms.io/isaving/models"
	constants "git.forms.io/isaving/sv/ssv0000008/constant"
	"git.forms.io/isaving/sv/ssv0000008/services"
	"git.forms.io/legobank/legoapp/constant"
	"git.forms.io/legobank/legoapp/controllers"
	"git.forms.io/legobank/legoapp/errors"
	"git.forms.io/universe/solapp-sdk/log"
	"runtime/debug"
)

type Ssv0000008Controller struct {
    controllers.CommController
}

func (*Ssv0000008Controller) ControllerName() string {
	return "Ssv0000008Controller"
}

// @Desc ssv0000008 controller
// @Author
// @Date 2020-12-12
func (c *Ssv0000008Controller) Ssv0000008() {

	defer func() {
		if r := recover(); r != nil {
			err := errors.Errorf(constant.SYSPANIC, "Ssv0000008Controller.Ssv0000008 Catch panic %v", r)
			log.Errorf("Error: %v, Stack: [%s]", err, debug.Stack())
			c.SetServiceError(err)
		}
	}()

	ssv0000008I := &models.SSV0000008I{}
	if err :=  models.UnPackRequest(c.Req.Body, ssv0000008I); err != nil {
		c.SetServiceError(errors.New(err, constants.ERRCODE2))
		return
	}
  if err := ssv0000008I.Validate(); err != nil {
		log.Errorf("Request message field validate failed, error:[%v]", err)
		c.SetServiceError(err)
		return
	}
	ssv0000008 := &services.Ssv0000008Impl{} 
    ssv0000008.New(c.CommController)
	ssv0000008.SSV0000008I = ssv0000008I

	ssv0000008O, err := ssv0000008.Ssv0000008(ssv0000008I)

	if err != nil {
		log.Errorf("Ssv0000008Controller.Ssv0000008 failed, err=%v", errors.ServiceErrorToString(err))
		c.SetServiceError(err)
		return
	}
	responseBody, err := ssv0000008O.PackResponse()
	if err != nil {
		c.SetServiceError(err)
		return
	}
	c.SetAppBody(responseBody)
}
// @Title Ssv0000008 Controller
// @Description ssv0000008 controller
// @Param Ssv0000008 body models.SSV0000008I true body for SSV0000008 content
// @Success 200 {object} models.SSV0000008O
// @router /create [post]
func (c *Ssv0000008Controller) SWSsv0000008() {
	//Here is to generate API documentation, no need to implement methods
}

//
// Copyright 2019 Shenzhen Forms Syntron Information Co., Ltd. All rights reserved.
//

package message

type TxnMessage struct {
	Id             uint64
	TopicAttribute map[string]string
	NeedReply      bool
	NeedAck        bool

	//app properties
	AppProps map[string]string

	//message payload
	Body string
}

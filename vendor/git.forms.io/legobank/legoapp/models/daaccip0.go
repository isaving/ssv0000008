package models

import (
	"git.forms.io/legobank/legoapp/constant"
	"git.forms.io/legobank/legoapp/errors"
	"git.forms.io/universe/common/json"
	"gopkg.in/go-playground/validator.v9"
)

type DAACCIP0I struct {
	IntPlanNo           string `validate:"required,max=20"`
	BankNo              string
	Currency            string
	IntFlag             string
	IntCalcOption       string
	MinIntAmt           float64
	LyrdFlag            string
	AmtLvlsFlag         string
	TermLvlsFlag        string
	TermAmtPrtyFlag     string
	IntRateUseFlag      string
	IntRateNo           string
	FixdIntRate         float64
	MinIntRate          float64
	MonIntUnit          string
	YrIntUnit           string
	FloatOption         string
	FloatFlag           string
	FloatCeil           float64
	FloatFloor          float64
	DefFloatRate        float64
	CornerFlag          string
	CritocalPointFlag   string
	LvlsIntRateAmtType  string
	TolLvls             int64
	LyrdTermUnit        string
	LoanIntRateAdjType  string
	LoanIntRateAdjCycle string
	LoanIntRateAdjFreq  string
	IntPlanDesc         string
	Flag1               string
	Flag2               string
	Back1               float64
	Back2               float64
	Back3               string
	LastMaintDate       string
	LastMaintTime       string
	LastMaintBrno       string
	LastMaintTell       string
}

type DAACCIP0O struct {
	IntPlanNo           string
	BankNo              string
	Currency            string
	IntFlag             string
	IntCalcOption       string
	MinIntAmt           float64
	LyrdFlag            string
	AmtLvlsFlag         string
	TermLvlsFlag        string
	TermAmtPrtyFlag     string
	IntRateUseFlag      string
	IntRateNo           string
	FixdIntRate         float64
	MinIntRate          float64
	MonIntUnit          string
	YrIntUnit           string
	FloatOption         string
	FloatFlag           string
	FloatCeil           float64
	FloatFloor          float64
	DefFloatRate        float64
	CornerFlag          string
	CritocalPointFlag   string
	LvlsIntRateAmtType  string
	TolLvls             int64
	LyrdTermUnit        string
	LoanIntRateAdjType  string
	LoanIntRateAdjCycle string
	LoanIntRateAdjFreq  string
	IntPlanDesc         string
	Flag1               string
	Flag2               string
	Back1               float64
	Back2               float64
	Back3               string
	LastMaintDate       string
	LastMaintTime       string
	LastMaintBrno       string
	LastMaintTell       string
}

type DAACCIP0IDataForm struct {
	FormHead CommonFormHead
	FormData DAACCIP0I
}

type DAACCIP0ODataForm struct {
	FormHead CommonFormHead
	FormData DAACCIP0O
}

type DAACCIP0RequestForm struct {
	Form []DAACCIP0IDataForm
}

type DAACCIP0ResponseForm struct {
	Form []DAACCIP0ODataForm
}

// @Desc Build request message
func (o *DAACCIP0RequestForm) PackRequest(DAACCIP0I DAACCIP0I) (responseBody []byte, err error) {

	requestForm := DAACCIP0RequestForm{
		Form: []DAACCIP0IDataForm{
			{
				FormHead: CommonFormHead{
					FormId: "DAACCIP0I",
				},
				FormData: DAACCIP0I,
			},
		},
	}

	responseBody, err = json.Marshal(requestForm)
	if err != nil {
		return nil, errors.Wrap(err, 0, constant.REQPACKERR)
	}

	return responseBody, nil
}

// @Desc Parsing request message
func (o *DAACCIP0RequestForm) UnPackRequest(request []byte) (DAACCIP0I, error) {
	DAACCIP0I := DAACCIP0I{}
	if err := json.Unmarshal(request, o); nil != err {
		return DAACCIP0I, errors.Wrap(err, 0, constant.REQUNPACKERR)
	}

	if len(o.Form) < 1 {
		return DAACCIP0I, errors.New("UnPackRequest failed.", constant.REQUNPACKERR)
	}

	return o.Form[0].FormData, nil
}

// @Desc Build response message
func (o *DAACCIP0ResponseForm) PackResponse(DAACCIP0O DAACCIP0O) (responseBody []byte, err error) {
	responseForm := DAACCIP0ResponseForm{
		Form: []DAACCIP0ODataForm{
			{
				FormHead: CommonFormHead{
					FormId: "DAACCIP0O",
				},
				FormData: DAACCIP0O,
			},
		},
	}

	responseBody, err = json.Marshal(responseForm)
	if err != nil {
		return nil, errors.Wrap(err, 0, constant.RSPPACKERR)
	}

	return responseBody, nil
}

// @Desc Parsing response message
func (o *DAACCIP0ResponseForm) UnPackResponse(request []byte) (DAACCIP0O, error) {

	DAACCIP0O := DAACCIP0O{}

	if err := json.Unmarshal(request, o); nil != err {
		return DAACCIP0O, errors.Wrap(err, 0, constant.RSPUNPACKERR)
	}

	if len(o.Form) < 1 {
		return DAACCIP0O, errors.New("UnPackResponse failed.", constant.RSPUNPACKERR)
	}

	return o.Form[0].FormData, nil
}

func (w *DAACCIP0I) Validate() error {
	validate := validator.New()
	return validate.Struct(w)
}

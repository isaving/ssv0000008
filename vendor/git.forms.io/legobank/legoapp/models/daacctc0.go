package models

import (
	"git.forms.io/legobank/legoapp/constant"
	"git.forms.io/legobank/legoapp/errors"
	"git.forms.io/universe/common/json"
	"gopkg.in/go-playground/validator.v9"
)

type DAACCTC0I struct {
	RecordNo            int64
	AcctgAcctNo         string `validate:"required,max=40"`
	BalanceType         string
	PeriodNum           int64
	CurrIntStDate       string
	CurrIntEndDate      string
	UnpaidInt           float64
	RepaidInt           float64
	AccmWdAmt           float64
	AccmIntSetlAmt      float64
	UnpaidIntAmt        float64
	RepaidIntAmt        float64
	AlrdyTranOffshetInt float64
	DcValueInt          float64
	CavInt              float64
	OnshetInt           float64
	FrzAmt              float64
	Status              string
	LastCalcDate        string
	LastMaintDate       string
	LastMaintTime       string
	LastMaintBrno       string
	LastMaintTell       string
	TccState            int8
	AcruUnstlIntr       float64
	AccmCmpdAmt         float64
}

type DAACCTC0O struct {
	RecordNo            int64
	AcctgAcctNo         string `validate:"required,max=40"`
	BalanceType         string
	PeriodNum           int64
	CurrIntStDate       string
	CurrIntEndDate      string
	UnpaidInt           float64
	RepaidInt           float64
	AccmWdAmt           float64
	AccmIntSetlAmt      float64
	UnpaidIntAmt        float64
	RepaidIntAmt        float64
	AlrdyTranOffshetInt float64
	DcValueInt          float64
	CavInt              float64
	OnshetInt           float64
	FrzAmt              float64
	Status              string
	LastCalcDate        string
	LastMaintDate       string
	LastMaintTime       string
	LastMaintBrno       string
	LastMaintTell       string
	TccState            int8
	AcruUnstlIntr       float64
	AccmCmpdAmt         float64
}

type DAACCTC0IDataForm struct {
	FormHead CommonFormHead
	FormData DAACCTC0I
}

type DAACCTC0ODataForm struct {
	FormHead CommonFormHead
	FormData DAACCTC0O
}

type DAACCTC0RequestForm struct {
	Form []DAACCTC0IDataForm
}

type DAACCTC0ResponseForm struct {
	Form []DAACCTC0ODataForm
}

// @Desc Build request message
func (o *DAACCTC0RequestForm) PackRequest(DAACCTC0I DAACCTC0I) (responseBody []byte, err error) {

	requestForm := DAACCTC0RequestForm{
		Form: []DAACCTC0IDataForm{
			{
				FormHead: CommonFormHead{
					FormId: "DAACCTC0I",
				},
				FormData: DAACCTC0I,
			},
		},
	}

	responseBody, err = json.Marshal(requestForm)
	if err != nil {
		return nil, errors.Wrap(err, 0, constant.REQPACKERR)
	}

	return responseBody, nil
}

// @Desc Parsing request message
func (o *DAACCTC0RequestForm) UnPackRequest(request []byte) (DAACCTC0I, error) {
	DAACCTC0I := DAACCTC0I{}
	if err := json.Unmarshal(request, o); nil != err {
		return DAACCTC0I, errors.Wrap(err, 0, constant.REQUNPACKERR)
	}

	if len(o.Form) < 1 {
		return DAACCTC0I, errors.New("UnPackRequest failed.", constant.REQUNPACKERR)
	}

	return o.Form[0].FormData, nil
}

// @Desc Build response message
func (o *DAACCTC0ResponseForm) PackResponse(DAACCTC0O DAACCTC0O) (responseBody []byte, err error) {
	responseForm := DAACCTC0ResponseForm{
		Form: []DAACCTC0ODataForm{
			{
				FormHead: CommonFormHead{
					FormId: "DAACCTC0O",
				},
				FormData: DAACCTC0O,
			},
		},
	}

	responseBody, err = json.Marshal(responseForm)
	if err != nil {
		return nil, errors.Wrap(err, 0, constant.RSPPACKERR)
	}

	return responseBody, nil
}

// @Desc Parsing response message
func (o *DAACCTC0ResponseForm) UnPackResponse(request []byte) (DAACCTC0O, error) {

	DAACCTC0O := DAACCTC0O{}

	if err := json.Unmarshal(request, o); nil != err {
		return DAACCTC0O, errors.Wrap(err, 0, constant.RSPUNPACKERR)
	}

	if len(o.Form) < 1 {
		return DAACCTC0O, errors.New("UnPackResponse failed.", constant.RSPUNPACKERR)
	}

	return o.Form[0].FormData, nil
}

func (w *DAACCTC0I) Validate() error {
	validate := validator.New()
	return validate.Struct(w)
}

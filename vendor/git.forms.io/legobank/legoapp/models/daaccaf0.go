package models

import (
	"git.forms.io/legobank/legoapp/constant"
	"git.forms.io/legobank/legoapp/errors"
	"git.forms.io/universe/common/json"
	"gopkg.in/go-playground/validator.v9"
)

type DAACCAF0I struct {
	HostTranSerialNo          string `validate:"required,max=42"`
	BussDate                  string
	BussTime                  string
	HostTranSeq               int64
	PeripheralSysWorkday      string
	PeripheralSysWorktime     string
	PeripheralTranSerialNo    string
	PeripheralTranSeq         float64
	BookedWorkday             string
	AcctingTime               string
	LiquidationDate           string
	LiquidationTime           string
	TranOrgId                 string
	AgentOrgId                string
	TranTeller                string
	AuthTeller                string
	ReviewTeller              string
	TranChannel               string
	AccessChannel             string
	TerminalNo                string
	BussSys                   string
	TrasactionCode            string
	FunctionCode              string
	ApprovalNo                string
	ClearingBussType          string
	ProdCode                  string
	ProdSeq                   float64
	BussType                  string
	BussCategories            string
	DebitFlag                 string
	LocalBankFlag             string
	Country                   string
	OtherBankFlag             string
	AcctingOrgId              string
	LocalAcctDiff             string
	LocalTranDetailType       string
	CustId                    string
	TheMerchantNo             string
	BussAcctNo                string
	AcctNo                    string
	AcctgAcctNo               string
	SubjectNo                 string
	SubjectBreakdown          string
	MediaType                 string
	LocalMediaPrefix          string
	MediaNo                   string
	CustDiff                  string
	AmtType                   string
	DurationOfDep             float64
	DaysOfDep                 float64
	AcctingCode1              string
	AcctingCode2              string
	AcctingCode3              string
	AcctingCode4              string
	EventIndication           string
	EventBreakdown            string
	CustEvent1                string
	CustEvent2                string
	Currency                  string
	BanknoteFlag              string
	Amt                       float64
	AcctBal                   float64
	CashTranFlag              string
	ValueDate                 string
	ExchRestType              string
	AntiTradingMark           string
	CorrectTradingFlag        string
	OriginalTradingDay        string
	OriginalHostTranSerialNo  float64
	OriginalTradeTellerNo     string
	AcctManagerNo             string
	MessageCode               string
	AbstractCode              string
	UseCode                   string
	SetNo                     string
	WhetherToAllowPunching    string
	ChannelCorrectControlFlag string
	IsSendClearingFlag        string
	IsMakeUpForPayables       string
	LastMaintDate             string
	LastMaintTime             string
	LastMaintBrno             string
	LastMaintTell             string
	BalanceType               string
	RepayPrincipal            float64
	RepayInterest             float64
	LastTranDate              string
}

type DAACCAF0O struct {
	Status	string
}

type DAACCAF0IDataForm struct {
	FormHead CommonFormHead
	FormData DAACCAF0I
}

type DAACCAF0ODataForm struct {
	FormHead CommonFormHead
	FormData DAACCAF0O
}

type DAACCAF0RequestForm struct {
	Form []DAACCAF0IDataForm
}

type DAACCAF0ResponseForm struct {
	Form []DAACCAF0ODataForm
}

// @Desc Build request message
func (o *DAACCAF0RequestForm) PackRequest(DAACCAF0I DAACCAF0I) (responseBody []byte, err error) {

	requestForm := DAACCAF0RequestForm{
		Form: []DAACCAF0IDataForm{
			{
				FormHead: CommonFormHead{
					FormId: "DAACCAF0I",
				},
				FormData: DAACCAF0I,
			},
		},
	}

	responseBody, err = json.Marshal(requestForm)
	if err != nil {
		return nil, errors.Wrap(err, 0, constant.REQPACKERR)
	}

	return responseBody, nil
}

// @Desc Parsing request message
func (o *DAACCAF0RequestForm) UnPackRequest(request []byte) (DAACCAF0I, error) {
	DAACCAF0I := DAACCAF0I{}
	if err := json.Unmarshal(request, o); nil != err {
		return DAACCAF0I, errors.Wrap(err, 0, constant.REQUNPACKERR)
	}

	if len(o.Form) < 1 {
		return DAACCAF0I, errors.New("UnPackRequest failed.", constant.REQUNPACKERR)
	}

	return o.Form[0].FormData, nil
}

// @Desc Build response message
func (o *DAACCAF0ResponseForm) PackResponse(DAACCAF0O DAACCAF0O) (responseBody []byte, err error) {
	responseForm := DAACCAF0ResponseForm{
		Form: []DAACCAF0ODataForm{
			{
				FormHead: CommonFormHead{
					FormId: "DAACCAF0O",
				},
				FormData: DAACCAF0O,
			},
		},
	}

	responseBody, err = json.Marshal(responseForm)
	if err != nil {
		return nil, errors.Wrap(err, 0, constant.RSPPACKERR)
	}

	return responseBody, nil
}

// @Desc Parsing response message
func (o *DAACCAF0ResponseForm) UnPackResponse(request []byte) (DAACCAF0O, error) {

	DAACCAF0O := DAACCAF0O{}

	if err := json.Unmarshal(request, o); nil != err {
		return DAACCAF0O, errors.Wrap(err, 0, constant.RSPUNPACKERR)
	}

	if len(o.Form) < 1 {
		return DAACCAF0O, errors.New("UnPackResponse failed.", constant.RSPUNPACKERR)
	}

	return o.Form[0].FormData, nil
}

func (w *DAACCAF0I) Validate() error {
	validate := validator.New()
	return validate.Struct(w)
}

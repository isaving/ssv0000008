package models

import (
	"encoding/base64"
	"fmt"
	"git.forms.io/RetailPlatform/common/kms/util"
	"git.forms.io/universe/common/crypto/aes"
	commRsa "git.forms.io/universe/common/crypto/rsa"
	"git.forms.io/universe/common/json"
	"github.com/astaxie/beego"
)

type KmsRequestCrypto struct {
	Request string
	Key     string
}

type KmsKeysRootResponse struct {
	Code      int    `json:"Code"`
	Error     string `json:"Error"`
	ServiceID string `json:"ServiceId"`
	Result    []KmsKeysRootResponseResult `json:"Result"`
}

type KmsKeysRootResponseResult struct {
	ServiceID   string `json:"ServiceId"`
	SystemType  string `json:"SystemType"`
	KeyType     string `json:"KeyType"`
	KeyID       string `json:"KeyId"`
	KeyVersion  int    `json:"KeyVersion"`
	KeyValue    string `json:"KeyValue"`
	State       int    `json:"State"`
	EffectTime  string `json:"EffectTime"`
	DeffectTime string `json:"DeffectTime"`
	Creator     string `json:"Creator"`
	CreateTime  string `json:"CreateTime"`
	Modifier    string `json:"Modifier"`
	LastUpdate  string `json:"LastUpdate"`
}

func PackRequestK1(readRequest interface{}, K1 []byte) ([]byte, error) {
	kmsReq, err := EncryptKmsRequest(readRequest, K1)
	if nil != err {
		return nil, err
	}

	dataByte, err := json.Marshal(kmsReq)
	//log.Debug("req json:" + string(dataByte))
	if nil != err {
		return nil, err
	}
	return dataByte, nil
}

func EncryptKmsRequest(data interface{}, K1 []byte) (KmsRequestCrypto, error) {
	kmsRequestCrypto := KmsRequestCrypto{}
	//rsa
	cryptoAlgorithm := "rsa"
	kmsPubKey, _ := util.FileLoad(beego.AppConfig.String("kms_key::keyPath"))
	cryptoIns := aes.NewAES("PKCS5", "GCM", K1)
	rsaInstance := commRsa.NewRSA(nil, kmsPubKey)
	s2, err := rsaInstance.Encrypt(K1)
	if nil != err {
		return kmsRequestCrypto, err
	}

	//2. 加密请求数据
	b, _ := json.Marshal(data)
	cipherText, err := cryptoIns.Encrypt(b)
	if nil != err {
		return kmsRequestCrypto, err
	}
	M1 := base64.StdEncoding.EncodeToString(cipherText)
	M2 := base64.StdEncoding.EncodeToString(s2)

	req := KmsRequestCrypto{
		Request: M1,
		Key:     M2,
	}
	fmt.Println("Request: " + M1)
	fmt.Println("Key: " + M2)
	fmt.Println("CryptoAlgorithm: " + cryptoAlgorithm)
	return req, nil
}

//func DecryptKmsResponse(rspBody []byte, K1 []byte) (dateByte []byte, err error) {
//	log.Debug("SendRequestReplyMsg Succ result=%v\n", json.Get(rspBody, "Response").ToString())
//	responseStr := json.Get(rspBody, "Response").ToString()
//
//	//写入返回结果中Response的值  responseStr := ""
//	cryptoIns := aes.NewAES("PKCS5", "GCM", K1)
//	//cryptoIns, err := sm4.NewSM4("CBC", SMK1)
//
//	preResponse, err := base64.StdEncoding.DecodeString(responseStr)
//	if nil != err {
//		return nil, err
//	}
//	originResponse, err := cryptoIns.Decrypt(preResponse)
//	if nil != err {
//		return nil, err
//	}
//
//	kmsKeysRootResponse := KmsKeysRootResponse{}
//	err = json.Unmarshal(originResponse, &kmsKeysRootResponse)
//	if nil != err {
//		return nil, err
//	}
//	log.Debug(kmsKeysRootResponse)
//
//	if kmsKeysRootResponse.Code != 0 {
//		return nil, errors.New(kmsKeysRootResponse.Message)
//	}
//
//	dataByte, err := json.Marshal(kmsKeysRootResponse.Data)
//	if nil != err {
//		return nil, err
//	}
//
//	return dataByte, nil
//}

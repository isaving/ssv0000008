//Version: v1.0.0
package services

import (
	"git.forms.io/isaving/models"
	constants "git.forms.io/isaving/sv/ssv0000008/constant"
	"git.forms.io/legobank/legoapp/constant"
	"git.forms.io/legobank/legoapp/errors"
	"git.forms.io/legobank/legoapp/services"
	"git.forms.io/universe/solapp-sdk/log"
)
type Ssv0000008Impl struct {
    services.CommonService
	SSV1000021O         *models.SSV1000021O
	SSV1000021I         *models.SSV1000021I
	SSV1000023O         *models.SSV1000023O
	SSV1000023I         *models.SSV1000023I

	SSV0000008I         *models.SSV0000008I
	SSV0000008O         *models.SSV0000008O
}
// @Desc Ssv0000008 process
// @Author
// @Date 2020-12-12
func (impl *Ssv0000008Impl) Ssv0000008(ssv0000008I *models.SSV0000008I) (ssv0000008O *models.SSV0000008O, err error) {

	impl.SSV0000008I = ssv0000008I

	//get sv Pub_Key
	if err = impl.SV100021(); nil != err {
		return nil, err
	}

	ssv0000008O = impl.SSV0000008O

	return ssv0000008O, nil
}

func (impl *Ssv0000008Impl) SV100021() error {
	Request := &models.SSV1000021I{
		KeyType : constants.RSA256PUB,
	}
	rspBody, err := Request.PackRequest()
	if nil != err {
		return errors.New(err, constants.ERRCODE1)
	}
	resBody, err := impl.RequestSyncServiceElementKey(constant.DLS_TYPE_CMM, constant.DLS_ID_COMMON, constants.SV100021, rspBody);
	if err != nil{
		log.Errorf("SV100021, err:%v",err)
		return err
	}
	model := &models.SSV0000008O{}
	if err :=model.UnPackResponse(resBody); nil != err {
		return errors.New(err, constants.ERRCODE2)
	}
	model.RecordNm = len(model.Result)
	impl.SSV0000008O = model
	return nil
}


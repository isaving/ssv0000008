////Version: v1.0.0
package routers

import (
	"git.forms.io/legobank/legoapp/constant"
	"git.forms.io/isaving/sv/ssv0000008/controllers"
	"git.forms.io/universe/common/event_handler/register"
	"github.com/astaxie/beego"
)

// @Desc Select the appropriate transaction route according to TOPIC
// @Author
// @Date 2020-12-12
func InitRouter() error {

	eventRouteReg := register.NewEventHandlerRegister()

	bc := beego.AppConfig

	eventRouteReg.Router(bc.String(constant.TopicPrefix + "ssv0000008"),
		&controllers.Ssv0000008Controller{}, "Ssv0000008")

	return nil
}

// @Desc transaction router for swagger
// @Author
// @Date 2020-12-12
func init() {

	ns := beego.NewNamespace("isaving/v1/xxxxxx",
		beego.NSNamespace("/xxxxxx",
			beego.NSInclude(
				&controllers.Ssv0000008Controller{},
			),
		),
	)
	beego.AddNamespace(ns)
}
